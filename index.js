const wordlist = require("wordle-wordlist");
const { rateDivisiveClues } = require('./threader.js');
const fs = require('fs');
const path = require('path');
const cacheLocation = path.join(__dirname, 'cache.json');

// For clue word enum:
const LETTER = 0;
const COLOR = 1;

function getCandidatesForGuess (clueWord, guesses) {
  const remainingCandidates = new Set();
  guesses.forEach(guess => remainingCandidates.add(guess));

  guesses.forEach((word) => {
    const wordSatisfiesGuess = checkWordWithGuess(word, clueWord);

    if (!wordSatisfiesGuess) {
      remainingCandidates.delete(word);
    }
  });
  return remainingCandidates;
}

function getCandidatesForGuesses (words, guesses) {
  words.forEach((word, i) => {
    guesses = getCandidatesForGuess(word, guesses);
  });
  return guesses;
}

function checkWordWithGuess (word, clueWord) {
  if (typeof clueWord[0] === 'string' || !Array.isArray(clueWord[0])) {
    console.trace('wrong type');
    throw new Error('clueWord should be an array of arrays')
  }
  for (let i = 0; i < clueWord.length; i++) {

    const passes = checkWordAgainstLetter(word, clueWord, i);
    if (!passes) {
      return false;
    }

  }
  return true;
}

function checkWordAgainstLetter (word, clueWord, i) {
  const letter = clueWord[i][LETTER];
  const color = clueWord[i][COLOR];
  const letters = word.split('');

  switch (color) {
    case 'green':

      if (letters[i] !== letter[0]) {
        return false;
      }
      break;

    case 'yellow':
      if (!word.includes(letter[0])) {
        return false
      }

      if (word.indexOf(letter[0]) === i) {
        return false
      }
      break;

    case 'grey':
      if (word.includes(letter[0])) {
        if (!isGreyPermitted(word, clueWord, i)) {
          return false;
        }
     }
     break;
  }
  return true;
}

function sortEliminatingWords (mostEliminatingWords) {

  const wordArr = [];
  for (let tuple of mostEliminatingWords) {
    wordArr.push(tuple);
  }

  const sorted = wordArr.sort((a, b) => {
    return a[1] - b[1];
  });

  return sorted;

}

function isGreyPermitted (word, clueWord, i) {
  const greyLetter = clueWord[i][LETTER];

  /**
   * For each letter in the canddiate `word`,
   * IF that letter is the grey letter (indicated by i)
   * This word is ONLY permitted if instances of that letter
   * at that index are green in the clueWord.
   */

  const split = word.split('');
  let nonGreenLetters = '';

  split.forEach((letter, x) => {
    if (clueWord[x][COLOR] !== 'green') {
      nonGreenLetters += letter;
    }
  });

  return !nonGreenLetters.includes(greyLetter);
}

function generateLetterFrequency (guesses) {
  const letters = 'abcdefghijklmnopqrstuvwxyz';
  const letterFrequency = {};
  letters.split('').forEach(l => letterFrequency[l] = 0);

  guesses.forEach((guess) => {
    for (let letter in letterFrequency) {
      if (guess.includes(letter)) {
        letterFrequency[letter]++;
      }
    }
  });
  return letterFrequency;
}


async function getGuesses () {
  try {
    const { guesses, answers } = JSON.parse(fs.readFileSync(cacheLocation).toString());
    return { guesses, answers }
  } catch (e) {
    const guesses = await wordlist.guesses();
    const answers = await wordlist.answers();
    fs.writeFileSync(cacheLocation, JSON.stringify({
      guesses,
      answers,
    }));
    return { guesses, answers };
  }
}

function generateMostEliminatingWords (guesses, letterFrequency) {
  const wordScores = new Map();
  Object.keys(letterFrequency).forEach((letter) => {
    guesses.forEach((word) => {
      if (word.includes(letter)) {
        if (!wordScores.has(word)) {
          wordScores.set(word, letterFrequency[letter]);
        } else {
          wordScores.set(word, letterFrequency[letter] + wordScores.get(word));
        }
      }
    });
  })
  return wordScores;
}

function rateEliminatingWordsBy (guesses) {
  const letterFrequency = generateLetterFrequency(guesses);
  const mostEliminatingWords = generateMostEliminatingWords(guesses, letterFrequency);
  const sortedEliminatingWords = sortEliminatingWords(mostEliminatingWords);
  return sortedEliminatingWords;
}

function generateClueFor (guess, answer) {
  const answerArr = answer.split('');
  return guess.split('').map((letter, i) => {
    if (letter === answerArr[i]) {
      return [letter, 'green'];
    } else if (!answer.includes(letter)) {
      return [letter, 'grey'];
    } else {
      return [letter, 'yellow'];
    }
  });
}

async function getIdealCandidatesForGuesses (clueWords, guesses, answers) {
  const remainingAnswers = getCandidatesForGuesses(clueWords, answers);
  return await rateDivisiveClues(guesses, remainingAnswers, false);
}

async function simulateStrategy (answer) {
  const { guesses, answers } = await getGuesses();
  const firstGuess = 'roate';
  const clue = generateClueFor(firstGuess, answer);
  const clues = [clue];
  let answerGuess;

  while (!answerGuess && !isAllGreen(clues[clues.length - 1])
        && clues.length < 5) {

    // check if we have an answer
    const remainingAnswers = getCandidatesForGuesses(clues, answers);
    if (remainingAnswers.size === 1) {
      clues.push(answer.split('').map(l => [l, 'green']));
      console.log(printMeme(clues));
      return clues;
    }

    // otherwise, narrow it down
    const candidates = await getIdealCandidatesForGuesses(clues, guesses, answers);
    const sorted = candidates.sort((a, b) => a[1] - b[1]);
    const bestGuess = sorted[0];
    const newClue = generateClueFor(bestGuess[0], answer);
    clues.push(newClue);
  }

  console.log(printMeme(clues));
  return clues;
}

function printMeme (clues) {
  return clues.map(word => word.map(emojiForColor).join('')).join('\n');
}

async function getIdealGuess2 (clues) {
  const { guesses, answers } = await getGuesses();
  let answerGuess;

  // check if we have an answer
  const remainingAnswers = getCandidatesForGuesses(clues, answers);
  if (remainingAnswers.size < 11 && remainingAnswers.size > 1) {
    console.log('There are few remaining answers:', remainingAnswers);
  }
  if (remainingAnswers.size === 1) {
    const iterator = remainingAnswers.entries();
    const answer = iterator.next().value[0];
    console.log(`The answer must be: ${answer}`);
    clues.push(answer.split('').map(l => [l, 'green']));
    console.log(printMeme(clues));
    return { answers: remainingAnswers };
  }

  // otherwise, narrow it down
  const candidates = await getIdealCandidatesForGuesses(clues, guesses, answers);
  const sorted = candidates.sort((a, b) => a[1] - b[1]);
  const bestGuess = sorted[0];
  console.log(`There are still ${remainingAnswers.size} possible answers.\nOur best guess is ${bestGuess[0]}, which will produce a clue that leaves ${bestGuess[1] / remainingAnswers.size} possibilities remaining on average.`);
  return { answers: remainingAnswers, bestGuess };
  return bestGuess;
}

function printMeme (clues) {
  return clues.map(word => word.map(emojiForColor).join('')).join('\n');
}


function emojiForColor (color) {
  switch (color[1]) {
    case 'green':
      return '🟩';
    case 'yellow':
      return '🟨';
    default:
      return '⬜';
  }
}

function isAllGreen (clue) {
  return clue.map(a => a[1]).filter(c => c === 'green').length === 5;
}

module.exports = {
  simulateStrategy,
  getGuesses,
  getCandidatesForGuess,
  getIdealCandidatesForGuesses,
  getCandidatesForGuesses,
  checkWordWithGuess,
  checkWordAgainstLetter,
  rateEliminatingWordsBy,
  isGreyPermitted,
  rateDivisiveClues,
  generateClueFor,
  getIdealGuess2,
}

