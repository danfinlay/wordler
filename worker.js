const { Worker, isMainThread, parentPort } = require('worker_threads');
const wordler = require('./');

parentPort.on('message', async ({ guess, guesses }) => {
  const divisive = [guess, 0];

  guesses.forEach((otherAnswer) => {
    if (otherAnswer === guess) return;

    const clue = wordler.generateClueFor(guess, otherAnswer);
    //console.log(`the resulting clue would be ${clue}`);
    const candidates = wordler.getCandidatesForGuess(clue, guesses);
    //console.log(`this would leave ${candidates.size} candidates.`);
    divisive[1] += candidates.size;
  });

  parentPort.postMessage({ type: 'result', body: divisive });

});


