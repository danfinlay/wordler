const test = require('tape');
const wordler = require('./');

test('against letter', (t) => {

  const res = wordler.checkWordAgainstLetter('syren', [
    [ 's', 'green' ],
    [ 'o', 'grey' ],
    [ 'a', 'grey' ],
    [ 'r', 'grey' ],
    [ 'e', 'grey' ]
  ], 0);

  t.ok(res, 'syren should pass this test');
  t.end();
});

test('getCandidatesForGuess', async (t) => {

  const expected = 231
  const guesses = await wordler.getGuesses();
  const result = wordler.getCandidatesForGuess([
    [ 's', 'green' ],
    [ 'o', 'grey' ],
    [ 'a', 'grey' ],
    [ 'r', 'grey' ],
    [ 'e', 'grey' ]
  ], guesses);

  console.log(result.size);
  t.equals(result.size, expected);
  t.end();
});

test('test getCandidatesForGuess with more greens', async (t) => {
  const initialGuesses = await wordler.getGuesses();
  const result = wordler.getCandidatesForGuess([
    [ 's', 'green' ],
    [ 'o', 'grey' ],
    [ 'a', 'green' ],
    [ 'r', 'grey' ],
    [ 'e', 'green' ]
  ], initialGuesses);

  console.log(`${result.size} options`);
  console.dir(result);

  const guesses = [];
  for (let word of result) {
    guesses.push(word);
  }
  console.dir(guesses);
  const mostEliminating = wordler.rateEliminatingWordsBy(guesses);
  console.dir(mostEliminating);

  t.end();
});

test('isGreyPermitted should permit', (t) => {

  const result = wordler.isGreyPermitted('soare', [
    [ 's', 'green' ],
    [ 'e', 'grey' ],
    [ 'a', 'green' ],
    [ 'm', 'grey' ],
    [ 'e', 'green' ]
  ], 1);

  t.ok(result, 'grey should be permitted');

  t.end();
});
test('isGreyPermitted when should not', (t) => {

  const result = wordler.isGreyPermitted('socle', [
    [ 's', 'grey' ],
    [ 'o', 'green' ],
    [ 'a', 'grey' ],
    [ 'r', 'grey' ],
    [ 'e', 'green' ]
  ], 0);

  t.ok(!result, 'grey should not be permitted');

  t.end();
});

test('test against letter', (t) => {

  const res = wordler.checkWordAgainstLetter('socle', [
    [ 's', 'grey' ],
    [ 'o', 'green' ],
    [ 'a', 'grey' ],
    [ 'r', 'grey' ],
    [ 'e', 'green' ]
  ], 0);

  t.ok(!res, 'socle should not pass this test');
  t.end();
});

/*
test('rateDivisiveClues', async (t) => {

  const guesses = await wordler.getGuesses();
  const divisive = wordler.rateDivisiveClues(guesses);

  t.end();

});
*/

test('test getCandidatesForGuesses', async (t) => {
  const words = await wordler.getGuesses();
  const initialGuesses = words.guesses;
  const result = wordler.getCandidatesForGuesses([
    [
      [ 's', 'grey' ],
      [ 'o', 'yellow' ],
      [ 'a', 'grey' ],
      [ 'r', 'green' ],
      [ 'e', 'grey' ]
    ],

  ], initialGuesses);

  const guesses = [];
  for (let word of result) {
    guesses.push(word);
  }
  console.log(`there are ${guesses.length} possibilities remaining`);
  const mostEliminating = wordler.rateEliminatingWordsBy(guesses);
  console.dir(mostEliminating[mostEliminating.length - 1]);

  t.end();
});

test('test simulateStrategy', async (t) => {
  const answers = [
    'thorn',
    'other',
    'tacit',
    'swill',
    'dodge',
    'shake',
    'caulk',
    'aroma',
    'cynic',
    'robin',
    'ultra',
    'ulcer',
    'pause',
    'humor',
    'frame',
    'elder',
    'skill',
    'aloft',
    'pleat',
    'shard',
    'moist',
    'those',
  ];

  for await (const answer of answers) {
    const result = await wordler.simulateStrategy(answer);
    console.log(`solved ${answer} in ${result.length} guesses`);
  }

  t.end();
});
/*
test('test getIdealCandidatesForGuesses', async (t) => {
  const { guesses, answers } = await wordler.getGuesses();
  const result = await wordler.getIdealGuess2([
    [
      [ 'r', 'grey' ],
      [ 'o', 'grey' ],
      [ 'a', 'grey' ],
      [ 't', 'grey' ],
      [ 'e', 'grey' ]
    ],
    [
      [ 's', 'grey' ],
      [ 'l', 'grey' ],
      [ 'i', 'yellow' ],
      [ 'm', 'grey' ],
      [ 'y', 'grey' ]
    ],
    [
      [ 'c', 'grey' ],
      [ 'h', 'grey' ],
      [ 'u', 'grey' ],
      [ 'f', 'grey' ],
      [ 'a', 'grey' ]
    ],

  ], guesses, answers);
  console.dir(result);

  console.dir(result.answers);

  t.end();
});


test('test simulateStrategy', async (t) => {
  const answer = 'vivid';
  const result = await wordler.simulateStrategy(answer);

  console.log('THE ANSWER');
  console.log(result);
  t.end();
});

*/
