const { rateDivisiveClues } = require('./threader.js');
const wordler = require('./');

(async function run () {

  const { guesses, answers } = await wordler.getGuesses();
  return rateDivisiveClues(guesses, answers);

})().catch(console.error)
.then(console.log);



