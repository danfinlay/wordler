'use strict';
const { join } = require('path');
const WorkerThread = require('worker_threads');
const fs = require('fs');
const path = require('path');
const concat = require('concat-iterator');
const progressFilePath = path.join(__dirname, 'progress.csv');

const THREAD_COUNT = 10;

const startTime = Date.now();

async function rateDivisiveClues (guesses, answers, useCache=true) {
  let isDone = false;

  let continuation = {};
  const howDivisive = [];
  const workers = [];
  const iterator = concat(answers[Symbol.iterator](), guesses[Symbol.iterator]());
  const done = new Set();

  if (useCache) {
    try {
      const previousResults = fs.readFileSync(progressFilePath).toString().split('\n')
      .map((line) => {
        return line.trim().split(',');
      });
      howDivisive.push(...previousResults);
      previousResults.forEach(res => done.add(res[0]));
    } catch (err) {
      console.log('no previous results found');
    }
  }

  function handleResult(i, divisive) {
    done.add(divisive);
    howDivisive.push(divisive);
    logTime();
    pushNext(workers[i]);
    if (useCache) {
      fs.appendFile(progressFilePath, `${divisive[0]},${divisive[1]}\n`, (err) => {
        if (err) {
          console.log('failed to record', divisive);
          console.error(err);
        }
      });
    }
  }

  function pushNext (worker) {
    let iteration = iterator.next();

    while (!iteration.done && done.has(iteration.value)) {
      iteration = iterator.next();
    }

    if (iteration.done) {
      worker.unref();
      if (done.size === (guesses.length + answers.size)) {
        writeResultToDisk(howDivisive);
        continuation.res(howDivisive);
        isDone = true;
      }
    } else {
      const guess = iteration.value;
      worker.postMessage({ guess, guesses: answers });
    }
  }

  function logTime () {
    const now = Date.now();
    const duration = now - startTime;
    const progress = howDivisive.length / guesses.length;
    const remain = duration / howDivisive.length * guesses.length;
  }

  function writeResultToDisk (howDivisive) {
    const outFilePath = join(__dirname, 'divisiveness.json');
    if (useCache) {
      fs.writeFileSync(outFilePath, JSON.stringify(howDivisive));
    }
  }

  //Spin up our initial batch of workers... we will reuse these workers by sending more work as they finish
  for (let j = 0; j < Math.min(THREAD_COUNT, guesses.length); j ++) {
    const worker = new WorkerThread.Worker(join(__dirname, './worker.js'));
    workers.push(worker);

    //Listen on messages from the worker
    worker.on('message', (messageBody) => {
      if (messageBody.type === 'result') {
        handleResult(j, messageBody.body);
      }
    });

    worker.on('error', console.error);

    pushNext(worker);
  }

  return new Promise((res, rej) => {
    continuation = { res, rej };
  });

}

module.exports = {
  rateDivisiveClues,
}

function mostDivisive (howDivisive) {
  const sorted = howDivisive.sort((a, b) => {
    return a[1] - b[1];
  });
  return {
    least: sorted[0],
    most: sorted[sorted.length - 1],
  }
}

