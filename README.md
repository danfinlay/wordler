# Wordler

A silly grab bag of wordle solving functions. Super sloppy, just for fun.
Currently uses a greedy strategy where for each guess it chooses the option that has the highest probability of reducing the outstanding possibilities the most. Seems to solve in just over 3 guesses on average.

## Installing

Pull the repo, have node.js installed, run `npm install`.

## Using the CLI

`node cli.js FIRST_GUESS FIRST_GUESS_RESULTS`

where `FIRST_GUESS` is the first guess you included, and `FIRST_GUESS_RESULTS` is a string representing the guessed color results, where:
- b = black/grey
- g = green
- y = yellow

Example:
`node cli.js roate bbgyg`
Suggests guessing `knish`.

You can then just add extra guesses & results as they come in:

Example:
```
$ node cli.js angle yybbb sandy byybb woman bbbyy
There are few remaining answers: Set(7) {
  'chant',
  'china',
  'crank',
  'frank',
  'ninja',
  'prank',
  'thank'
}
There are still 7 possible answers.
Our best guess is capri, which will produce a clue that leaves 1 possibilities remaining on average.
```

## Using the library

I recommend referring to `test.js` for various examples of using the utility functions that are mostly exposed by `index.js`.

The heaviest-weight function lives in `threader.js`, which is just named so because it spawns up multiple `worker.js` instances, and asks each of them to determine how many possibilities could exist from the remaining possible answers given the resulting clues for that guess. It's a mouthfull. A simple way to imagine its output is "the average number of possibilities remaining after you make this guess, times all possible correct answers".

## How it Fares

Here are some past performance examples. You can see how it would do on hypothetical examples using the `await wordler.simulateStrategy(answer)` method. It produces nice printouts like this:

```
🟨🟨⬜🟨⬜
⬜🟨⬜⬜🟨
🟩🟩🟩🟩🟩
solved thorn in 3 guesses
🟨🟨⬜🟨🟨
🟩🟩🟩🟩🟩
solved other in 2 guesses
⬜⬜🟨🟨⬜
🟨⬜🟨⬜🟩
🟩🟩🟩🟩🟩
solved tacit in 3 guesses
⬜⬜⬜⬜⬜
🟩🟨🟩⬜⬜
⬜⬜⬜⬜⬜
🟩🟩🟩🟩🟩
solved swill in 4 guesses
⬜🟩⬜⬜🟩
🟨⬜⬜⬜⬜
🟩🟩🟩🟩🟩
solved dodge in 3 guesses
⬜⬜🟩⬜🟩
⬜⬜⬜🟨🟨
🟩⬜🟩🟩🟩
🟩🟩🟩🟩🟩
solved shake in 4 guesses
⬜⬜🟨⬜⬜
🟨⬜⬜⬜⬜
🟩🟩⬜🟨🟨
🟩🟩🟩🟩🟩
solved caulk in 4 guesses
🟨🟨🟨⬜⬜
🟨🟨⬜🟨⬜
🟩🟩🟩🟩🟩
solved aroma in 3 guesses
⬜⬜⬜⬜⬜
⬜⬜🟨⬜🟨
🟩🟩🟩🟩🟩
solved cynic in 3 guesses
🟩🟩⬜⬜⬜
⬜⬜🟨⬜⬜
🟩🟩🟩🟩🟩
solved robin in 3 guesses
🟨⬜🟨🟨⬜
⬜⬜⬜🟨🟨
🟩🟩🟩🟩🟩
solved ultra in 3 guesses
🟨⬜⬜⬜🟨
⬜🟨⬜🟨⬜
🟨🟨🟨🟩🟨
🟩🟩🟩🟩🟩
solved ulcer in 4 guesses
⬜⬜🟨⬜🟩
⬜🟩🟩⬜🟨
🟨🟩🟩⬜🟩
🟩🟩🟩🟩🟩
solved pause in 4 guesses
🟨🟨⬜⬜⬜
⬜🟩⬜⬜⬜
⬜🟩🟨🟩🟩
🟩🟩🟩🟩🟩
solved humor in 4 guesses
🟨⬜🟩⬜🟩
⬜🟩🟩⬜⬜
⬜🟨⬜🟨⬜
🟩🟩🟩🟩🟩
solved frame in 4 guesses
🟨⬜⬜⬜🟨
🟨🟨⬜🟨⬜
🟩🟩🟩🟩🟩
solved elder in 3 guesses
⬜⬜⬜⬜⬜
🟩🟨🟩⬜⬜
⬜⬜⬜⬜🟨
🟩🟩🟩🟩🟩
solved skill in 4 guesses
⬜🟨🟨🟨⬜
⬜🟩🟩🟨🟩
🟩🟩🟩🟩🟩
solved aloft in 3 guesses
⬜⬜🟨🟨🟨
⬜🟩🟩🟩🟩
🟩🟩🟩🟩🟩
solved pleat in 3 guesses
🟨⬜🟩⬜⬜
🟨⬜⬜⬜🟨
🟩🟩🟩🟩🟩
solved shard in 3 guesses
⬜🟩⬜🟨⬜
🟩⬜⬜🟨⬜
🟩🟩🟩🟩🟩
solved moist in 3 guesses
⬜🟨⬜🟨🟩
🟨⬜⬜⬜⬜
🟩🟩🟩🟩🟩
solved those in 3 guesses
```

