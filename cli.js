#! /usr/bin/env/node

const wordler = require('./');
const args = process.argv;
args.shift() // node
args.shift() // cli.js

const clues = cluesFromArgs(args);
guess(clues).then(console.log)
.catch(console.error);

function cluesFromArgs (args) {
  const clues = [];

  if (args.length === 0) {
    throw new Error(`For a first guess, I recommend "roate". Reaching that conclusion is pre-baked right now, because it takes a while to reach it.`);
    return [];
  }

  while (args.length > 1) {

    const letters = args.shift().split('');
    const colorLetters = args.shift().split('');

    const clue = [];

    letters.forEach((letter, i) => {
      clue.push([
        letter,
        colorForLetter(colorLetters[i]),
      ]);
    });
    clues.push(clue);
  }

  return clues;
}

function colorForLetter(letter) {
  switch (letter) {
    case 'b':
      return 'grey';
    case 'g':
      return 'green';
    case 'y':
      return 'yellow';
  }
  throw new Error('valid color letters are b, g, and y.');
}


async function guess(clues) {
  const { guesses, answers } = await wordler.getGuesses();
  const result = await wordler.getIdealGuess2(clues, guesses, answers);
};

