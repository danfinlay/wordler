const fs = require('fs');
const path = require('path');

const progress = fs.readFileSync(path.join(__dirname, 'raw.csv')).toString().split('\n')
.filter((row) => {
  if (row.indexOf('Divisive') === 0) {
    return false
  }
  if (row.indexOf('Completed') === 0) {
    return false
  }
  if (row.indexOf('pushing') === 0) {
    return false
  }

  if (row.indexOf("'") === 0) {
    return false
  }
  return true;

})
.map(row => {
  return eval(row)
})
.map((row) => {
  return row
})

progress.forEach((row) => {
  if (typeof row === 'undefined') {
    return
  }
  console.log(`${row[0]},${row[1]}`);
});

